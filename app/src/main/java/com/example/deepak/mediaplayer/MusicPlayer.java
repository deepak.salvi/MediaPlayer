package com.example.deepak.mediaplayer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import java.io.IOException;


public class MusicPlayer extends AppCompatActivity implements View.OnClickListener{

    static MediaPlayer mediaPlayer;

    String[] mySongs;
    int position;
    Uri uri;
    Thread updateSeekBar;

    SeekBar seekBar;
    Button btPlay, btFF, btFB, btNext, btPrev;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);


        seekBar = (SeekBar) findViewById(R.id.seekBar);
        btPlay = (Button) findViewById(R.id.btPlay);
        btFF = (Button) findViewById(R.id.btFF);
        btFB = (Button) findViewById(R.id.btFB);
        btNext = (Button) findViewById(R.id.btNext);
        btPrev = (Button) findViewById(R.id.btPrev);



        btPlay.setOnClickListener(this);
        btFF.setOnClickListener(this);
        btFB.setOnClickListener(this);
        btNext.setOnClickListener(this);
        btPrev.setOnClickListener(this);

        updateSeekBar = new Thread(){
            @Override
            public void run() {

                int totalDuration = mediaPlayer.getDuration();
                int currentPosition = 0;


                while (currentPosition < totalDuration){
                    try {
                        sleep(500);
                        currentPosition = mediaPlayer.getCurrentPosition();
                        seekBar.setProgress(currentPosition);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }

                //super.run();
            }
        };

        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mySongs = bundle.getStringArray("songList");
        position = bundle.getInt("position", 0);

         uri = Uri.parse(mySongs[position]);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (position < mySongs.length) {
                    mediaPlayer.reset();
       /* load the new source */
                    try {
                        mediaPlayer.setDataSource(mySongs[position + 1]);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
       /* Prepare the mediaplayer */
                    try {
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
       /* start */
                    mediaPlayer.start();
                    seekBar.setMax(mediaPlayer.getDuration());
                } else {
       /* release mediaplayer */
                    mediaPlayer.release();
                }
            }
        });

       /* mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (position < mySongs.length) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    position = (position + 1)%mySongs.length;
                    uri = Uri.parse(mySongs[position]);
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                    mediaPlayer.start();
                    seekBar.setMax(mediaPlayer.getDuration());
                } else {

                    mediaPlayer.release();
                }
            }
        });*/

        seekBar.setMax(mediaPlayer.getDuration());

        updateSeekBar.start();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_music_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        mediaPlayer.release();
    }
    @Override
    public  void onClick(View v){
        int id = v.getId();
        switch (id){
            case R.id.btPlay :
                if (mediaPlayer.isPlaying()){
                    btPlay.setText(">");
                    mediaPlayer.pause();
                }
                else {
                    btPlay.setText("||");
                    mediaPlayer.start();
                }
                break;
            case R.id.btFF :
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);

                break;

            case  R.id.btFB :
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
                break;

            case R.id.btNext :
                mediaPlayer.stop();
                mediaPlayer.release();
                position = (position + 1)%mySongs.length;
                 uri = Uri.parse(mySongs[position]);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());

                break;

            case R.id.btPrev :
                mediaPlayer.stop();
                mediaPlayer.release();
                position = (position - 1)<0 ? mySongs.length-1:position-1;
                 uri = Uri.parse(mySongs[position]);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                mediaPlayer.start();
                seekBar.setMax(mediaPlayer.getDuration());
                break;
        }
    }
}
