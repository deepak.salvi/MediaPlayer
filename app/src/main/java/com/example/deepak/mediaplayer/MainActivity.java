package com.example.deepak.mediaplayer;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.io.File;


public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private String[] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);


        final String[] mySongs = getMusicList();
        File[] file = new File[mySongs.length];
        items = new String[mySongs.length];
        for(int i = 0; i<mySongs.length;i++){

            file[i] = new File(mySongs[i]);
            items[i] = file[i].getName().toString().replace(".mp3", "");

        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.song_layout, R.id.textView, items);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), MusicPlayer.class).putExtra("position", position).putExtra("songList", mySongs));
            }
        });
    }


    private String[] getMusicList()
    {
        final Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[] { MediaStore.Audio.Media.DATA }, null,null, "LOWER(" + MediaStore.Audio.Media.DATA + ") ASC");

        int count = cursor.getCount();

        String[] songs = new String[count];

        int i = 0;

        if (cursor.moveToFirst())
        {
            do
            {
                songs[i] = cursor.getString(0);
                i++;
            } while (cursor.moveToNext());
        }

        return songs;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
